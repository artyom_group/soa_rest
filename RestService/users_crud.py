from flask import Flask, request, send_file, jsonify
from models import User, Base
import sqlalchemy
from sqlalchemy.orm import sessionmaker
import json
from werkzeug.utils import secure_filename
import os
import pika
from sqlalchemy_utils import create_database
ALLOWED_EXTENSIONS = {'jpg'}


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='rabbit'))
channel = connection.channel()
channel.queue_declare(queue='work_queue')
channel.queue_declare(queue='response')

engine = sqlalchemy.create_engine("postgresql://postgres:root@db/rest")
Session = sessionmaker(bind=engine)
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = "images/"


@app.route("/users", methods=["GET"])
def get_users():
    with Session() as session, session.begin():
        res = session.query(User)
        return jsonify([item.to_dict() for item in res])


@app.route("/users", methods=["POST"])
def add_user():
    with Session() as session, session.begin():
        if len(request.data) == 0:
            data = {}
        else:
            data = json.loads(request.data)
        user = User(gender=data.get("gender", None), email=data.get("email", None))
        session.add(user)
        session.flush()
        return jsonify(id=user.id, gender=user.gender, email=user.email)


@app.route("/users/<user_id>", methods=["GET"])
def get_user_by_id(user_id):
    with Session() as session, session.begin():
        user = session.query(User).filter_by(id=user_id).one_or_none()
        if user is None:
            return jsonify({"text": "User not found"}), 404
        return jsonify(id=user.id, gender=user.gender, email=user.email)


@app.route("/users/<user_id>", methods=["PUT"])
def change_user(user_id):
    with Session() as session, session.begin():
        try:
            data = json.loads(request.data)
        except json.decoder.JSONDecodeError:
            return jsonify("Bad request"), 400
        user = session.query(User).filter_by(id=user_id).one_or_none()
        if user is None:
            return jsonify({"text": "Not Found"}), 404
        user.email = data["email"] if "email" in data else user.email
        user.gender = data["gender"] if "gender" in data else user.gender
        return {"text": "Change complete"}, 201


@app.route("/users/<user_id>", methods=["DELETE"])
def delete_user(user_id):
    with Session() as session, session.begin():
        user = session.query(User).filter_by(id=user_id).one_or_none()
        if user is None:
            return jsonify({"text": "User not found"}), 404
        if user.userpic:
            os.remove(os.path.join(app.config['UPLOAD_FOLDER'], user.userpic))
        stm = sqlalchemy.delete(User).where(User.id == user_id)
        session.execute(stm)
        return {"text": "Delete complete"}, 200


@app.route("/users/<user_id>/userpic", methods=["PUT"])
def set_userpic(user_id):
    with Session() as session, session.begin():
        user = session.query(User).filter_by(id=user_id).one_or_none()
        if user is None:
            return jsonify({"text": "Not found"}), 404
        if 'file' not in request.files:
            return jsonify({"text": "No file selected"}), 400
        file = request.files['file']
        if file.filename == '':
            return jsonify({"text": "No file selected 2"}), 400
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            user.userpic = filename
            return jsonify({"text": "Change complete"}), 201
        return jsonify({"text": "Bad file extension"}), 400


@app.route("/users/<user_id>/userpic", methods=["GET"])
def get_userpic(user_id):
    with Session() as session, session.begin():
        user = session.query(User).filter_by(id=user_id).one_or_none()
        if user is None:
            return jsonify({"text": "Not found"}), 404
        if user.userpic is None:
            return jsonify({"text": "Userpic not found"}), 404
        return send_file(os.path.join(app.config['UPLOAD_FOLDER'], user.userpic), mimetype='image/jpg')


@app.route("/users/<user_id>/userpic", methods=["DELETE"])
def delete_userpic(user_id):
    with Session() as session, session.begin():
        user = session.query(User).filter_by(id=user_id).one_or_none()
        if user is None:
            return jsonify({"text": "Not found"}), 404
        if user.userpic is None:
            return jsonify({"text": "Userpic not found"}), 404
        os.remove(os.path.join(app.config['UPLOAD_FOLDER'], user.userpic))
        user.userpic = None
        return jsonify({"text": "Image deleted"}), 200


@app.route("/statistic/<user_id>")
def generate_statistics(user_id):
    with Session() as session, session.begin():
        user = session.query(User).filter_by(id=user_id).one_or_none()
        if user is None:
            return jsonify({"text": "Not found"}), 404
        filename = str(user_id) + ".pdf"
        data = {
            "filename": filename,
            "email": user.email,
            "gender": user.gender,
            "number_of_sessions": user.number_of_sessions,
            "wins": user.wins,
            "loses": user.loses,
            "time": user.overall_time_minutes
        }
        channel.basic_publish(exchange='', routing_key='work_queue', body=json.dumps(data))
        return jsonify({"url": "/statistics/" + filename})


@app.route("/statistics/<filename>")
def get_pdf(filename):
    if filename not in os.listdir('pdfs'):
        return jsonify({"text": "File not created yet"}), 404
    return send_file("pdfs/" + filename)


if __name__ == "__main__":
    create_database("postgresql://postgres:root@db/rest")
    engine = sqlalchemy.create_engine("postgresql://postgres:root@db/rest")
    Base.metadata.create_all(bind=engine)
    app.run(host='0.0.0.0', port=80)
