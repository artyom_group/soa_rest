import sqlalchemy
from sqlalchemy.orm import declarative_base
from sqlalchemy_utils import database_exists, create_database
Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    gender = sqlalchemy.Column(sqlalchemy.String(10))
    email = sqlalchemy.Column(sqlalchemy.String(50))
    userpic = sqlalchemy.Column(sqlalchemy.String(100))
    number_of_sessions = sqlalchemy.Column(sqlalchemy.Integer, default=0)
    wins = sqlalchemy.Column(sqlalchemy.Integer, default=0)
    loses = sqlalchemy.Column(sqlalchemy.Integer, default=0)
    overall_time_minutes = sqlalchemy.Column(sqlalchemy.Integer, default=0)

    def to_dict(self):
        return {
            "id": self.id,
            "gender": self.gender,
            "email": self.email
        }


if __name__ == "__main__":
    create_database("postgresql://postgres:root@db/rest")
    engine = sqlalchemy.create_engine("postgresql://postgres:root@db/rest")
    Base.metadata.create_all(bind=engine)
