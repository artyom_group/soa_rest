import pika
import sys
import os
import json

from borb.pdf import Document
from borb.pdf import Page
from borb.pdf import SingleColumnLayout
from borb.pdf import Paragraph
from borb.pdf import PDF

def main():
    print("Worker started")
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbit'))
    channel = connection.channel()

    channel.queue_declare(queue='work_queue')
    channel.queue_declare(queue='responses')

    def callback(ch, method, properties, body):
        data = json.loads(body.decode())
        print("Got", data["filename"])
        pdf = Document()
        page = Page()
        layout = SingleColumnLayout(page)
        layout.add(Paragraph("email = " + str(data["email"])))
        layout.add(Paragraph("gender = " + str(data["gender"])))
        layout.add(Paragraph("number of sessions = " + str(data["number_of_sessions"])))
        layout.add(Paragraph("wins = " + str(data["wins"])))
        layout.add(Paragraph("loses = " + str(data["loses"])))
        layout.add(Paragraph("total time = " + str(data["time"])))
        pdf.append_page(page)

        with open("pdfs/" + data["filename"], "wb") as f:
            PDF.dumps(f, pdf)


    channel.basic_consume(queue='work_queue', on_message_callback=callback, auto_ack=True)
    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
